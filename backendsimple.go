package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

// Handling post
func PostMethod(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "Error Coy",
				http.StatusInternalServerError)
		}
		databyte := []byte(body)
		var data map[string]interface{}
		json.Unmarshal(databyte, &data)
		json.NewEncoder(w).Encode(data)
	} else {
		http.Error(w, "Yuhu salah Method", http.StatusMethodNotAllowed)
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/post", PostMethod)
	log.Fatal(http.ListenAndServe(":"+"9000", mux))
}
