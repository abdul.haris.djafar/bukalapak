FROM golang:1.8-alpine
COPY backendsimple.go .
RUN go build backendsimple.go
EXPOSE 9000
CMD ./backendsimple

